import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_stripe/flutter_stripe.dart';

class PaymentPage extends StatelessWidget {
  const PaymentPage({Key? key}) : super(key: key);

  final apiUrl = 'http://192.168.178.94:8000/v1';
  final billingDetails = const BillingDetails(
    name: 'Mario Bros',
    email: 'm.quercia@besanfra.com',
    phone: '+39 339 12 34 567',
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          CardField(
            onCardChanged: (card) {
              print(card);
            },
          ),
          TextButton(
            onPressed: () async {
              try {
                // 1. Create PaymentMethod
                // final paymentMethod = await Stripe.instance.createPaymentMethod(const PaymentMethodParams.card());
                final paymentMethod = await Stripe.instance.createPaymentMethod(PaymentMethodParams.card(
                  paymentMethodData: PaymentMethodData(
                    billingDetails: billingDetails
                  )
                ));
                print(paymentMethod);

                // 2. Create PaymentIntent from backend API
                final paymentIntent = await _fetchPaymentIntent();
                print(paymentIntent);

                // 3a. Confirm payment with 'paymentMethod.id' and 'paymentIntent['clientSecret']'
                final confirmPayment = await Stripe.instance.confirmPayment(
                  paymentIntent['client_secret'],
                  PaymentMethodParams.cardFromMethodId(
                    paymentMethodData: PaymentMethodDataCardFromMethod(
                      paymentMethodId: paymentMethod.id,
                    ),
                  ),
                );
                // 3b. Confirm payment with 'paymentMethod' and 'paymentIntent['clientSecret']'
                // final confirmPayment = await Stripe.instance.confirmPayment(
                //   paymentIntent['client_secret'],
                //   PaymentMethodParams.card(
                //     billingDetails: billingDetails,
                //   ),
                // );
                print(confirmPayment);

                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text('Success!: The payment was confirmed successfully!')));

              } catch (e) {
                ScaffoldMessenger.of(context)
                    .showSnackBar(SnackBar(content: Text('Error: $e')));
                rethrow;
              }
            },
            child: const Text('pay'),
          )
        ],
      ),
    );
  }

  Future<Map<String, dynamic>> _fetchPaymentIntent() async {
    final url = Uri.parse('$apiUrl/users/booking_payment_intent/');
    final response = await http.post(
      url,
      headers: {
        // 'Authorization': idToken,
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: json.encode({
        'amount': 1.99,
        'customerId': 'cus_H5bWPckPx4i7sU',
        'applicationFeeAmount': 1.00,
        'merchantId': 'acct_1IizgeR3dEyuTALO',
        'uid': 'weRuTLTxJRM2meipKjyQG35ed9G2',
        'nameUser': 'Mario Bros',
        'startDate': 1640908800,
        'endDate': 1640908800,
        'reservationCode': 'JUD1HL',
        'resortId': 65,
        'reservationType': 'booking'
      }),
    );
    return json.decode(response.body);
  }
}
