import 'package:flutter/material.dart';
import 'package:flutter_stripe/flutter_stripe.dart';
import 'package:stripe_app/app.dart';
import 'package:stripe_app/env.dart';

// PROJECT REFERENCE: https://blog.logrocket.com/exploring-stripe-flutter-sdk/
// PROJECT REFERENCE: https://github.com/flutter-stripe/flutter_stripe/blob/main/example/lib/screens/card_payments/webhook_payment_screen.dart
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // set the publishable key for Stripe - this is mandatory
  Stripe.publishableKey = stripePublishableKey;

  runApp(const App());
}
